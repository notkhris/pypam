#FROM registry.redhat.io/ubi8/ubi
FROM fedora:30
RUN dnf update -y \
    && dnf install python36 -y \
    && dnf clean all \
    && rm -rf /var/cache/yum

WORKDIR /app

COPY . /app

RUN pip3 install --no-cache-dir .

EXPOSE 6543
CMD pserve production.ini
