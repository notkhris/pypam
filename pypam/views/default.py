from pyramid.view import view_config
from pyramid.httpexceptions import HTTPFound
from pypam.ipa.connection import get_zones, get_records, inspect_record, new_record

from pypam.forms import NewRecordForm

@view_config(route_name='home',
             renderer='../templates/mytemplate.jinja2')
def home(request):
    return {}


@view_config(route_name='zones',
             renderer='../templates/zones.jinja2')
def zones(request):
    zones = get_zones()
    return {'zones': zones}


@view_config(route_name='records',
             renderer='../templates/records.jinja2')
def records(request):
    zone = request.matchdict['zone']
    records = get_records(zone)
    return {'records': records, 'zone': zone}


@view_config(route_name='get_record',
             renderer='../templates/record.jinja2')
def get_record(request):
    zone = request.matchdict['zone']
    record = request.matchdict['record']
    detail = inspect_record(zone, record)
    return {'record': detail, 'zone': zone}


@view_config(route_name='add_record',
             renderer='../templates/add_record.jinja2')
def add_record(request):
    form = NewRecordForm()
    zone = request.matchdict['zone']

    if request.method == "GET":
        zone = request.matchdict['zone']
        form = NewRecordForm()
        return {'form': form, 'zone': zone}

    if request.method == "POST":
        submitted = NewRecordForm(request.POST)
        if submitted.validate():
            zone = request.matchdict['zone']
            recordname = request.POST['dnsname']
            recorddata = request.POST['data']
            recordtype = request.POST['recordtype']
            new_record(zone, recordname, recordtype, recorddata)

            request.session.flash('Record sucsessfully added.')
            return HTTPFound(request.route_url('records', zone=zone))
        else:
            request.session.flash('Unable to validate IP. Please try again.')
            return {'form': form, 'zone': zone}
