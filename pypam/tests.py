import unittest

from pyramid import testing


class ViewTests(unittest.TestCase):
    def setUp(self):
        self.config = testing.setUp()

    def tearDown(self):
        testing.tearDown()

#    def test_my_view(self):
#        from .views.default import home
#        request = testing.DummyRequest()
#        info = home(request)
#        self.assertEqual(info['project'], 'pypam')


class FunctionalTests(unittest.TestCase):
    def setUp(self):
        from pypam import main
        app = main({})
        from webtest import TestApp
        self.testapp = TestApp(app)

    def test_root(self):
        res = self.testapp.get('/', status=200)
        self.assertTrue(b'PyPAM' in res.body)

 #   def test_zones(self):
 #       res = self.testapp.get('/zones', status=200)
 #       self.assertTrue(b'Current Zones' in res.body)

    def test_404(self):
        res = self.testapp.get('/myfakeurl', status=404)
        self.assertEqual('404 Not Found', res.status)
