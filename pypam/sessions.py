from pyramid.session import SignedCookieSessionFactory
session_factory = SignedCookieSessionFactory('mysupersecret')

def includeme(config):
    config.set_session_factory(session_factory)
