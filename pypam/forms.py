from wtforms import Form, BooleanField, StringField, SelectField, SubmitField, validators


class NewRecordForm(Form):
    dnsname = StringField('dnsname', [validators.InputRequired(message='You must enter a hostname')])
    recordtype = SelectField('recordtype', choices=[('arecord', 'arecord'), ('aaaa', 'ipv6 record')])
    data = StringField('data', [validators.IPAddress(message='The IP address was not understood')])
    submit = SubmitField()
