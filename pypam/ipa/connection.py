from python_freeipa import ClientLegacy
import configparser
import os


def new_client():
    ipa = configparser.ConfigParser()
    config = ('freeipa.ini')
    ipa.read(config)
    conn = ClientLegacy(os.environ.get('PYPAM_IPASERVER',
                                 default=ipa['server']['hostname']))
    conn.login(
            os.environ.get('PYPAM_IPAUSER',
                           default=ipa['client']['user']),
            os.environ.get('PYPAM_IPAPASS',
                           default=ipa['client']['password']))
    return conn


def get_zones():
    client = new_client()
    zones = []
    res = client.dnszone_find(**{'pkey_only': 'true'})
    for each in res.get('result'):
        zones.append(each['idnsname'][0])
    return zones


def get_records(zone):
    client = new_client()
    records = []
    res = client.dnsrecord_find(zone, **{'pkey_only': 'true'})
    for each in res.get('result'):
        records.append(each['idnsname'][0])
    return records


def inspect_record(zone, record):
    client = new_client()
    record = client.dnsrecord_show(zone, record, **{'raw': 'true'})
    if 'dn' in record:
        record.pop('dn')
    if 'objectClass' in record:
        record.pop('objectClass')
    return record

def new_record(zone, recordname, recordtype, recorddata):
    client = new_client()
    client.dnsrecord_add(zone, recordname, **{recordtype: recorddata})

