def includeme(config):
    config.add_static_view('static', 'static', cache_max_age=3600)
    config.add_route('home', '/')
    config.add_route('zones', '/zones')
    config.add_route('records', '/zones/{zone}')
    config.add_route('add_record', '/zones/{zone}/add')
    config.add_route('get_record', '/zones/{zone}/{record}')
