pypam
=====

Getting Started
---------------

- Change directory into your newly created project.

    cd pypam

- Create a Python virtual environment.

    python3 -m venv env

- Upgrade packaging tools.

    env/bin/pip install --upgrade pip setuptools

- Install the project in editable mode with its testing requirements.

    env/bin/pip install -e ".[testing]"

- Run your project's tests.

    env/bin/pytest

- Run your project.

    env/bin/pserve development.ini

Docker
======

Environment variables
---------------------
`PYPAM_IPASERVER`	# freeipa url

`PYPAM_IPAUSER`		# freeipa user with read/write DNS privileges

`PYPAM_IPAPASS`		# password of freeipa user

`REQUESTS_CA_BUNDLE`	# local CA certificate to import to requests; neccessary if freeipa is using a self-signed certificate 
			
Alternatively, you can set these under the `[app:main]` section of `production.ini`
